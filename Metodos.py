from deap import base, creator, tools, algorithms
import random
import numpy as np
import cv2
import os

def correct_illumination(image, alpha=1.2, beta=20):
    return cv2.convertScaleAbs(image, alpha=alpha, beta=beta)

def filter_noise(image, kernel_size):
    if kernel_size <= 0:
        kernel_size = 1  # o cualquier otro número impar mayor que cero
    elif kernel_size % 2 == 0:
        kernel_size += 1  # hacerlo impar
    return cv2.GaussianBlur(image, (kernel_size, kernel_size), 0)


def enhance_contrast(image, clipLimit=3.0, tileGridSize=(10, 10)):
    clahe = cv2.createCLAHE(clipLimit=clipLimit, tileGridSize=tileGridSize)
    channels = cv2.split(image)
    clahe_channels = [clahe.apply(ch) for ch in channels]
    return cv2.merge(clahe_channels)

def detect_edges(image, low_threshold=30, high_threshold=90):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return cv2.Canny(gray, low_threshold, high_threshold)

def additional_smoothing(image, kernel_size):
    if kernel_size <= 0:
        kernel_size = 1  # o cualquier otro número impar mayor que cero
    elif kernel_size % 2 == 0:
        kernel_size += 1  # hacerlo impar
    return cv2.GaussianBlur(image, (kernel_size, kernel_size), 0)


def threshold_segmentation(image, lower_bound, upper_bound):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    
    # Asegurarse de que lower_bound y upper_bound sean del mismo tipo de datos
    lower_bound = np.array(lower_bound, dtype=np.uint8)
    upper_bound = np.array(upper_bound, dtype=np.uint8)
    
    mask = cv2.inRange(hsv, lower_bound, upper_bound)
    return mask


def jaccard_index(mask, reference_mask):
    intersection = np.logical_and(mask, reference_mask)
    union = np.logical_or(mask, reference_mask)
    return np.sum(intersection) / np.sum(union)

def random_odd(min_val, max_val):
    return random.choice([x for x in range(min_val, max_val + 1) if x % 2 == 1])

def custom_mutate(individual, mu, sigma, indpb):
    tools.mutGaussian(individual, mu, sigma, indpb)
    # Asegurarse de que kernel_size_smooth sea impar y mayor que cero
    if individual[7] <= 0:
        individual[7] = 1  # o cualquier otro número impar mayor que cero
    elif individual[7] % 2 == 0:
        individual[7] += 1  # hacerlo impar

