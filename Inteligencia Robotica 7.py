import random
import numpy as np
import cv2
import os
from deap import base, creator, tools, algorithms
from functools import partial



# Definición de la estructura de los individuos
creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", list, fitness=creator.FitnessMax)
# Ajustar estas rutas según tus necesidades
ruta_imagenes = R"C:\Users\Kevin\Documents\train\granulatorio3"
ruta_mascaras = R"C:\Users\Kevin\Documents\train\granulatoriolabels3"

def equal(image, clipLimit, tileGridSize, min_area, cv2thresh_binary):
    # Convertir a escala de grises
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Aplicar ecualización de histograma adaptativa
    clahe = cv2.createCLAHE(clipLimit=clipLimit, tileGridSize=tileGridSize)
    equalized = clahe.apply(gray)
    # Aplicar umbralización
    _, thresholded = cv2.threshold(equalized, 0, 255, cv2thresh_binary + cv2.THRESH_OTSU)
    # Encontrar contornos
    contours, _ = cv2.findContours(thresholded, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # Máscara para eliminar áreas pequeñas
    mask = np.zeros_like(thresholded)
    for contour in contours:
        if cv2.contourArea(contour) >= min_area:
            cv2.drawContours(mask, [contour], -1, (255), thickness=cv2.FILLED)
    # Aplicar máscara a la imagen
    result = cv2.bitwise_and(image, image, mask=mask)
    return result

def equal2(image, clipLimit2, tileGridSize2, min_area2, cv2thresh_binary2):
    # Convertir a escala de grises
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Aplicar ecualización de histograma adaptativa
    clahe = cv2.createCLAHE(clipLimit=clipLimit2, tileGridSize=tileGridSize2)
    equalized = clahe.apply(gray)
    # Aplicar umbralización
    _, thresholded = cv2.threshold(equalized, 0, 255, cv2thresh_binary2 + cv2.THRESH_OTSU)
    # Encontrar contornos
    contours, _ = cv2.findContours(thresholded, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # Máscara para eliminar áreas pequeñas
    mask = np.zeros_like(thresholded)
    for contour in contours:
        if cv2.contourArea(contour) >= min_area2:
            cv2.drawContours(mask, [contour], -1, (255), thickness=cv2.FILLED)
    # Aplicar máscara a la imagen
    result = cv2.bitwise_and(image, image, mask=mask)
    return result

def filter(image, conversion, low_value1, high_value1, low_value2, high_value2):
    # Convertir la imagen al espacio de color deseado
    converted = cv2.cvtColor(image, conversion)
    # Crear máscaras para los rangos de valores
    mask1 = cv2.inRange(converted, np.array(low_value1), np.array(high_value1))
    mask2 = cv2.inRange(converted, np.array(low_value2), np.array(high_value2))
    # Combinar máscaras
    combined_mask = cv2.add(mask1, mask2)
    # Aplicar máscara a la imagen original
    result = cv2.bitwise_and(image, image, mask=combined_mask)
    return result, converted

def mutate(individual):
    # Definir límites para los diferentes tipos de parámetros
    float_min, float_max = 1.0, 4.0  # Ejemplo para parámetros tipo float
    int_min, int_max = 1, 380  # Ejemplo para valores enteros dentro de una tupla

    for i, param in enumerate(individual):
        if isinstance(param, float):
            if random.random() < 0.2:
                individual[i] += random.gauss(0, 1)
                individual[i] = max(float_min, min(individual[i], float_max))
        elif isinstance(param, tuple):
            if random.random() < 0.2:
                individual[i] = tuple(
                    x + random.gauss(0, 1) if random.random() < 0.5 else x for x in param
                )
                individual[i] = tuple(max(int_min, min(x, int_max)) for x in individual[i])
        # Continúa con más condiciones si es necesario

    return individual,

#def procesar_y_guardar_imagenes(individual, directorio_imagenes, directorio_mascaras, directorio_salida):
#    imagenes, mascaras = cargar_imagenes_y_mascaras(directorio_imagenes, directorio_mascaras)

#    for i, imagen in enumerate(imagenes):
#        imagen_procesada = aplicar_procesamiento(imagen, individual)
#        ruta_salida = os.path.join(directorio_salida, f"imagen_procesada_{i}.png")
#        cv2.imwrite(ruta_salida, imagen_procesada)

# Función para crear un individuo
def individual():
    clipLimit = random.uniform(1.0, 3.0)
    tileGridSize = (random.randint(5, 5), random.randint(5, 5))
    min_area = random.randint(200, 400)
    cv2thresh_binary = random.choice([cv2.THRESH_BINARY, cv2.THRESH_BINARY_INV])
    clipLimit2 = random.uniform(2.0, 4.0)
    tileGridSize2 = (random.randint(1, 1), random.randint(1, 1))
    min_area2 = random.randint(200, 400)
    cv2thresh_binary2 = random.choice([cv2.THRESH_BINARY, cv2.THRESH_BINARY_INV])
    low_high_values = [random.randint(0, 255) for _ in range(12)]  # 12 valores para los rangos de `filter`
    return creator.Individual([clipLimit, tileGridSize, min_area, cv2thresh_binary] + [clipLimit2, tileGridSize2, min_area2, cv2thresh_binary2]+ low_high_values)

# Función para cargar imágenes y máscaras
def cargar_imagenes_y_mascaras(directorio_imagenes, directorio_mascaras):
    imagenes = []
    mascaras = []
    for archivo in sorted(os.listdir(directorio_imagenes)):
        if archivo.endswith(".jpg") or archivo.endswith(".png"):
            ruta_imagen = os.path.join(directorio_imagenes, archivo)
            ruta_mascara = os.path.join(directorio_mascaras, archivo)

            imagen = cv2.imread(ruta_imagen)
            mascara = cv2.imread(ruta_mascara, 0)

            # Verificación adicional aquí
            if imagen is None:
                print(f"Error al cargar la imagen: {ruta_imagen}")
                continue
            if mascara is None:
                print(f"Error al cargar la máscara: {ruta_mascara}")
                continue

            imagenes.append(imagen)
            mascaras.append(mascara)

    return imagenes, mascaras

# Función para calcular el índice de Jaccard
def calcular_indice_jaccard(imagen_procesada, mascara_referencia):
    if imagen_procesada is None or mascara_referencia is None:
        raise ValueError("La imagen procesada o la máscara de referencia es None.")

    # Convertir la imagen procesada a escala de grises si tiene tres canales
    if len(imagen_procesada.shape) == 3:
        imagen_procesada = cv2.cvtColor(imagen_procesada, cv2.COLOR_BGR2GRAY)

    # Asegurarse de que las imágenes tienen la misma forma
    if imagen_procesada.shape != mascara_referencia.shape:
        raise ValueError("Las formas de la imagen procesada y la máscara de referencia no coinciden.")

    # Calcular intersección y unión
    interseccion = np.logical_and(imagen_procesada, mascara_referencia)
    union = np.logical_or(imagen_procesada, mascara_referencia)
    indice_jaccard = np.sum(interseccion) / np.sum(union) if np.sum(union) > 0 else 0

    return indice_jaccard

# Función de evaluación
def evalJaccard(individual, directorio_imagenes, directorio_mascaras):
    imagenes, mascaras = cargar_imagenes_y_mascaras(directorio_imagenes, directorio_mascaras)
    jaccard_scores = []

    for imagen, mascara in zip(imagenes, mascaras):
        imagen_procesada = aplicar_procesamiento(imagen, individual)  # Implementa esta función según tus necesidades
        score = calcular_indice_jaccard(imagen_procesada, mascara)
        jaccard_scores.append(score)

    return np.mean(jaccard_scores),

def aplicar_procesamiento(imagen, individual):
    clipLimit, tileGridSize, min_area, cv2thresh_binary, clipLimit2, tileGridSize2, min_area2, cv2thresh_binary2, *low_high_values = individual
    tileGridSize = (int(tileGridSize[0]), int(tileGridSize[1]))
    tileGridSize2 = (int(tileGridSize2[0]), int(tileGridSize2[1]))
    low_value1 = low_high_values[:3]
    high_value1 = low_high_values[3:6]
    low_value2 = low_high_values[6:9]
    high_value2 = low_high_values[9:12]

    # Aplicar la función equal
    firt_equal = equal(imagen, clipLimit, tileGridSize, min_area, cv2thresh_binary)
    # Aplicar la función filter
    first_filter, hsv = filter(firt_equal, cv2.COLOR_BGR2HSV, low_value1, high_value1, low_value2, high_value2)
    # Aplicar de nuevo la función equal
    second_equal = equal2(first_filter, clipLimit2, tileGridSize2, min_area2, cv2thresh_binary2)
    _, imagen_binarizada = cv2.threshold(second_equal, 50, 255, cv2.THRESH_BINARY)
    return imagen_binarizada

# Configuración del algoritmo genético
toolbox = base.Toolbox()
toolbox.register("individual", individual)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)
toolbox.register("evaluate", evalJaccard)
toolbox.register("mate", tools.cxOnePoint)
toolbox.register("mutate", mutate)  # Asegúrate de definir esta función
toolbox.register("select", tools.selTournament, tournsize=3)
toolbox.register("evaluate", partial(evalJaccard, directorio_imagenes=ruta_imagenes, directorio_mascaras=ruta_mascaras))

# Función principal
def main():
    pop = toolbox.population(n=300)  # Tamaño de la población
    hof = tools.HallOfFame(1)  # Almacena solo el mejor individuo
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("max", np.max)

    algorithms.eaSimple(pop, toolbox, cxpb=0.7, mutpb=0.5, ngen=100, stats=stats, halloffame=hof, verbose=True)

    mejor_individuo = hof[0]
    mejor_fitness = mejor_individuo.fitness.values[0]

    print("Mejor individuo:", mejor_individuo)
    print("Mejor índice de Jaccard:", mejor_fitness)

    # Directorio donde guardar las imágenes procesadas
    #directorio_salida = R"C:\Users\Kevin\Documents\train\mejor2"

    # Procesar y guardar imágenes con el mejor individuo
    #procesar_y_guardar_imagenes(mejor_individuo, ruta_imagenes, ruta_mascaras, directorio_salida)

    return pop, stats, hof, mejor_individuo, mejor_fitness

if __name__ == "__main__":
    _, _, _, mejor_individuo, mejor_fitness = main()

    