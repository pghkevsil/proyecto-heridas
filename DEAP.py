from deap import base, creator, tools, algorithms
import random
import numpy as np
import cv2

def corregir_iluminacion(image, alpha=1.2, beta=20):
    return cv2.convertScaleAbs(image, alpha=alpha, beta=beta)

def filtro_ruido(image, tamaño_kernel):
    if tamaño_kernel <= 0:
        tamaño_kernel = 1  # o cualquier otro número impar mayor que cero
    elif tamaño_kernel % 2 == 0:
        tamaño_kernel += 1  # hacerlo impar
    return cv2.GaussianBlur(image, (tamaño_kernel, tamaño_kernel), 0)

def mejorar_contraste(image, clipLimit=3.0, tileGridSize=(10, 10)):
    clahe = cv2.createCLAHE(clipLimit=clipLimit, tileGridSize=tileGridSize)
    channels = cv2.split(image)
    clahe_channels = [clahe.apply(ch) for ch in channels]
    return cv2.merge(clahe_channels)

def detectar_bordes(image, low_threshold=30, high_threshold=90):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return cv2.Canny(gray, low_threshold, high_threshold)

def suavizado_adicional(image, tamaño_kernel):
    if tamaño_kernel <= 0:
        tamaño_kernel = 1  # o cualquier otro número impar mayor que cero
    elif tamaño_kernel % 2 == 0:
        tamaño_kernel += 1  # hacerlo impar
    return cv2.GaussianBlur(image, (tamaño_kernel, tamaño_kernel), 0)

def segmentación_umbral(image, limite_inferior, limite_superior):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    
    # limite_inferior y limite_superior sean del mismo tipo de datos
    limite_inferior = np.array(limite_inferior, dtype=np.uint8)
    limite_superior = np.array(limite_superior, dtype=np.uint8)
    
    mascara = cv2.inRange(hsv, limite_inferior, limite_superior)
    return mascara

def índice_jaccard(mascara, reference_mascara):
    intersection = np.logical_and(mascara, reference_mascara)
    union = np.logical_or(mascara, reference_mascara)
    return np.sum(intersection) / np.sum(union)

def random_odd(min_val, max_val):
    return random.choice([x for x in range(min_val, max_val + 1) if x % 2 == 1])

def custom_mutate(individual, mu, sigma, indpb):
    tools.mutGaussian(individual, mu, sigma, indpb)
    # Asegurarse de que tamaño_kernel_smooth sea impar y mayor que cero
    if individual[7] <= 0:
        individual[7] = 1  # o cualquier otro número impar mayor que cero
    elif individual[7] % 2 == 0:
        individual[7] += 1  # hacerlo impar

# Función de evaluación para el algoritmo genético
def evaluate(individual):
    alpha, beta, tamaño_kernel_noise, clipLimit, tileGridSize, low_threshold, high_threshold, tamaño_kernel_smooth, lower_h, upper_h = individual

    #print(f"Evaluating with tamaño_kernel_noise: {tamaño_kernel_noise}")  # Debugging line

    tileGridSize = int(round(tileGridSize))

    tamaño_kernel_noise = int(round(tamaño_kernel_noise))
    tamaño_kernel_noise = tamaño_kernel_noise if tamaño_kernel_noise % 2 == 1 else tamaño_kernel_noise + 1

    tamaño_kernel_smooth = int(round(tamaño_kernel_smooth))
    tamaño_kernel_smooth = tamaño_kernel_smooth if tamaño_kernel_smooth % 2 == 1 else tamaño_kernel_smooth + 1

    # Aplicar métodos de procesamiento de imágenes
    corrected_image = corregir_iluminacion(image, alpha, beta)
    filtered_image = filtro_ruido(corrected_image, tamaño_kernel_noise)
    enhanced_image = mejorar_contraste(filtered_image, clipLimit, (tileGridSize, tileGridSize))
    edges = detectar_bordes(enhanced_image, low_threshold, high_threshold)
    #print("Debugging tamaño_kernel_smooth:", tamaño_kernel_smooth)
    smoothed_edges = suavizado_adicional(edges, tamaño_kernel_smooth)

    # Segmentación por umbral
    limite_inferior = np.array([lower_h, 100, 100])
    limite_superior = np.array([upper_h, 255, 255])
    mascara = segmentación_umbral(enhanced_image, limite_inferior, limite_superior)
    cv2.imwrite(R"C:\Users\Kevin\Documents\train\labels\mejor.png",mascara)
    # Calcular el coeficiente de Jaccard
    jaccard = índice_jaccard(mascara, reference_mascara)
    return jaccard,

if __name__ == "__main__":
    # Cargar la imagen y la máscara de referencia
    image = cv2.imread(R"C:\Users\Kevin\Documents\train\images\0070.png")
    reference_mascara = cv2.imread(R"C:\Users\Kevin\Documents\train\labels\0070.png", cv2.IMREAD_GRAYSCALE)

    # Configuración del algoritmo genético
    creator.create("FitnessMax", base.Fitness, weights=(1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMax)

    toolbox = base.Toolbox()
    # Rangos de los parámetros ajustados
    toolbox.register("attr_float", random.uniform, 0.8, 1.2)  # alpha
    toolbox.register("attr_int", random.randint, 10, 30)  # beta
    toolbox.register("attr_odd", random_odd, 5, 9)  # tamaño_kernel_noise
    toolbox.register("attr_float", random.uniform, 2.0, 4.0)  # clipLimit
    toolbox.register("attr_int", random.randint, 8, 12)  # tileGridSize
    toolbox.register("attr_int", random.randint, 40, 80)  # low_threshold
    toolbox.register("attr_int", random.randint, 100, 150)  # high_threshold
    toolbox.register("attr_odd_smooth", random_odd, 3, 11)  # tamaño_kernel_smooth
    toolbox.register("attr_int", random.randint, 0, 5)  # lower_h
    toolbox.register("attr_int", random.randint, 10, 15)  # upper_h
    toolbox.register("mutate", custom_mutate, mu=0, sigma=1, indpb=0.2)
    toolbox.register("individual", tools.initCycle, creator.Individual, 
                 (toolbox.attr_float, toolbox.attr_int, toolbox.attr_odd, toolbox.attr_float, toolbox.attr_int, toolbox.attr_int, toolbox.attr_int, toolbox.attr_odd_smooth, toolbox.attr_int, toolbox.attr_int), 
                 n=1)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("mate", tools.cxBlend, alpha=0.5)
    toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=1, indpb=0.2)
    toolbox.register("select", tools.selTournament, tournsize=3)
    toolbox.register("evaluate", evaluate)
    
    # Tamaño de la población ajustado
    population = toolbox.population(n=200)

    # Número de generaciones ajustado
    ngen = 100

    # Probabilidades de cruzamiento y mutación ajustadas
    probab_crossing, probab_mutating = 0.7, 0.3

    hof = tools.HallOfFame(1)

    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("max", np.max)
    # Ejecución del algoritmo genético
    algorithms.eaSimple(population, toolbox, probab_crossing, probab_mutating, ngen, stats=stats, halloffame=hof, verbose=True)
    
    best_ind = hof[0]
    evaluate(best_ind)
    print("Mejor individuo es: %s, %s" % (best_ind, best_ind.fitness.values))
