from deap import base, creator, tools, algorithms
from multiprocessing import Pool
import random
import numpy as np
import cv2
import os

# Función para corregir la iluminación de una imagen
def corregir_iluminacion(image, alpha=1.2, beta=20):
    return cv2.convertScaleAbs(image, alpha=alpha, beta=beta)

# Función para aplicar un filtro de ruido a la imagen
def filtro_ruido(image, tamaño_kernel):
    # Asegurarse de que el tamaño del kernel sea impar y mayor que cero
    if tamaño_kernel <= 0:
        tamaño_kernel = 1  # o cualquier otro número impar mayor que cero
    elif tamaño_kernel % 2 == 0:
        tamaño_kernel += 1  # hacerlo impar
    return cv2.GaussianBlur(image, (tamaño_kernel, tamaño_kernel), 0)

# Función para mejorar el contraste de la imagen
def mejorar_contraste(image, clipLimit=3.0, tileGridSize=(10, 10)):
    clahe = cv2.createCLAHE(clipLimit=clipLimit, tileGridSize=tileGridSize)
    channels = cv2.split(image)
    clahe_channels = [clahe.apply(ch) for ch in channels]
    return cv2.merge(clahe_channels)

# Función para segmentar una imagen basada en un umbral de color
def segmentación_umbral(image, limite_inferior, limite_superior):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    # Conversión de límites a uint8(revisar)
    limite_inferior = np.array(limite_inferior, dtype=np.uint8)
    limite_superior = np.array(limite_superior, dtype=np.uint8)
    mascara = cv2.inRange(hsv, limite_inferior, limite_superior)
    return mascara

# Función para calcular el índice Jaccard, que mide la similitud entre dos máscaras
def coeficiente_sorensen_dice(mascara, reference_mascara):
    intersection = np.logical_and(mascara, reference_mascara)
    return 2.0 * np.sum(intersection) / (np.sum(mascara) + np.sum(reference_mascara))

# Función para obtener un número impar aleatorio entre dos valores
def random_odd(min_val, max_val):
    return random.choice([x for x in range(min_val, max_val + 1) if x % 2 == 1])

# Función personalizada de mutación
def custom_mutate(individual, mu, sigma, indpb):
    tools.mutGaussian(individual, mu, sigma, indpb)
    return (individual,)

# Función para evaluar un individuo en base a su capacidad para procesar imágenes
def evaluate_individual(individual, images_dir, masks_dir):
    best_dice = -1  # Valor inicial para el mejor coeficiente de Dice.
    best_image_path = None
    best_mask = None
    total_dice = 0  # Suma total de los coeficientes de Dice.
    num_images = 0

    for image_file in os.listdir(images_dir):
        if image_file.endswith(('.png', '.jpg', '.jpeg')):
            image_path = os.path.join(images_dir, image_file)
            mask_path = os.path.join(masks_dir, image_file)
            dice_coefficient, mask = evaluate(individual, image_path, mask_path)  # Modificado para usar Coeficiente de Dice.
            
            if dice_coefficient > best_dice:
                best_dice = dice_coefficient
                best_image_path = image_path
                best_mask = mask

            total_dice += dice_coefficient
            num_images += 1
    
    if best_mask is not None:
        filename = os.path.basename(best_image_path)
        output_path = os.path.join(R"C:\Users\Kevin\Documents\train\mejor", f"best_mask_for_{filename}")
        cv2.imwrite(output_path, best_mask)

    return total_dice / num_images,


# Función para evaluar la población de forma paralela y mejorar la eficiencia
def parallel_evaluate(population, images_dir, masks_dir):
    with Pool(processes=8) as pool:
        args = [(ind, images_dir, masks_dir) for ind in population]
        fitnesses = pool.starmap(evaluate_individual, args)
    return fitnesses

# Función de evaluación principal del algoritmo genético
def evaluate(individual, image_path, reference_mask_path):
    image = cv2.imread(image_path)
    reference_mascara = cv2.imread(reference_mask_path, cv2.IMREAD_GRAYSCALE)
    alpha, beta, tamaño_kernel, clipLimit, tileGridSize, lower_h, upper_h = individual

    # Acota el valor de tileGridSize después de la mutación y antes de usarlo
    tileGridSize = max(8, min(12, int(round(tileGridSize))))

    tamaño_kernel = int(round(tamaño_kernel))
    tamaño_kernel = tamaño_kernel if tamaño_kernel % 2 == 1 else tamaño_kernel + 1

    # Aplicar métodos de procesamiento de imágenes
    corrected_image = corregir_iluminacion(image, alpha, beta)
    filtered_image = filtro_ruido(corrected_image, tamaño_kernel)
    enhanced_image = mejorar_contraste(filtered_image, clipLimit, (tileGridSize, tileGridSize))
    #print("Dimensiones de la imagen después de mejorar_contraste:", enhanced_image.shape)

    #edges = detectar_bordes(enhanced_image, low_threshold, high_threshold)
    
    #print("Debugging tamaño_kernel_smooth:", tamaño_kernel_smooth)
    #smoothed_edges = suavizado_adicional(edges, tamaño_kernel_smooth)

    # Segmentación por umbral
    limite_inferior = np.array([lower_h, 100, 100])
    limite_superior = np.array([upper_h, 255, 255])
    mascara = segmentación_umbral(enhanced_image, limite_inferior, limite_superior)
    # Calcular el coeficiente de Jaccard
    dice = coeficiente_sorensen_dice(mascara, reference_mascara)
    return dice, mascara  # Agregado el retorno de la máscara.


# Configuración y ejecución del algoritmo genético
if __name__ == "__main__":
    # Directorio de las imágenes y máscaras
    images_dir = R"C:\Users\Kevin\Documents\train\granulatorio3"
    masks_dir = R"C:\Users\Kevin\Documents\train\granulatoriolabels3"

    # Configuración del algoritmo genético
    creator.create("FitnessMax", base.Fitness, weights=(1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMax)

    toolbox = base.Toolbox()
    # Rangos de los parámetros ajustados
    toolbox.register("attr_float", random.uniform, 0.5, 2) # alpha
    toolbox.register("attr_int", random.randint, 0, 130) # beta

    toolbox.register("attr_odd", random_odd, 3, 55) # tamaño_kernel_noise

    toolbox.register("attr_float", random.uniform, 1.0, 5.0) # clipLimit

    toolbox.register("attr_int", random.randint, 3, 32) # tileGridSize

    toolbox.register("attr_int", random.randint, 50, 200) # lower_h   

    toolbox.register("attr_int", random.randint, 150, 255) # upper_h
    toolbox.register("individual", tools.initCycle, creator.Individual, 
                 (toolbox.attr_float, toolbox.attr_int, toolbox.attr_odd, toolbox.attr_float, toolbox.attr_int, toolbox.attr_int, toolbox.attr_int), 
                 n=1)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("mate", tools.cxOnePoint)
    toolbox.register("mutate", custom_mutate, mu=0, sigma=1, indpb=0.2)
    toolbox.register("select", tools.selTournament, tournsize=3)
    toolbox.register("evaluate", evaluate_individual, images_dir=images_dir, masks_dir=masks_dir)
    # Tamaño de la población ajustado
    population = toolbox.population(n=400)

    # Número de generaciones ajustado
    ngen = 100

    # Probabilidades de cruzamiento y mutación ajustadas
    probab_crossing, probab_mutating = 0.7, 0.3

    # Inicialización de la "Sala de la Fama"
    hof = tools.HallOfFame(1)
    # Configuración de las estadísticas para recopilar el valor máximo de fitness
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("max", np.max)

    # Evaluación paralela de la población inicial.
    fitnesses = parallel_evaluate(population, images_dir, masks_dir)
    for ind, fit in zip(population, fitnesses):
        ind.fitness.values = fit
        
    # Ejecución del algoritmo genético
    algorithms.eaSimple(population, toolbox, probab_crossing, probab_mutating, ngen, stats=stats, halloffame=hof, verbose=True)

    # Recuperación y evaluación del mejor individuo tras la ejecución del algoritmo.
    best_ind = hof[0]
    best_jaccard = toolbox.evaluate(best_ind)[0]
    print(f"Mejor Jaccard: {best_jaccard}, Parámetros: {best_ind}")